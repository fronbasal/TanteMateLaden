#!/bin/bash
ssh -J tantemate@space.chaospott.de tantemate@10.42.0.223 << EOF
 source venv/bin/activate
 cd TanteMateLaden
 git pull
 git submodule update
 cd TanteMateLaden
 python manage.py collectstatic --noinput
 python manage.py makemigrations
 python manage.py migrate
 pkill -HUP -F uwsgi.pid
EOF